1. Run `screen_collector.py`.
2. Go to https://cdn.rawgit.com/fernozzle/yt-comments/master/yt-comments.html, change the API key and video ID, and run. Place the output in `./_in/comments.json`.
3. Convert to JSON with `screens_to_json.py` or calculate results with `parse_votes.py`.

Find output files in `./_out/`.