import pytesseract
from PIL import Image
from difflib import get_close_matches
import json
pytesseract.pytesseract.tesseract_cmd = r"C:/Program Files/Tesseract-OCR/tesseract.exe"

KEYWORD_BOX_COORDS = (1600, 20, 1900, 120)
KEYWORD_BOX_COORDS_LARGE = (1500, 0, 1920, 140)
N_SCREENS = 1352

N_RESPONSES_PER_SCREEN = 6
RESPONSE_OFFSET = (140, 290)
RESPONSE_TEXT_HEIGHT = 40
RESPONSE_GAP_HEIGHT = 110

RESPONSE_SAMPLE_WIDTH = 120

SCREENS_FOLDER = "24a_voting_screens"

screens_data_file = open("./_out/screens.txt", "r")
screens_data_file_2 = open("./screens.txt", "w")

screens_data_file_2.write(screens_data_file.readline())
for i, line in enumerate(screens_data_file):
	[keyword, responses] = line.rstrip("\n").split(" ")

	if not keyword:
		# Get the current voting screen
		voting_screen_image_path = "./{}/{}.png".format(SCREENS_FOLDER, str(i).zfill(4))
		voting_screen_image = Image.open(voting_screen_image_path)

		keyword_image = voting_screen_image.crop(KEYWORD_BOX_COORDS_LARGE)
		keyword = pytesseract.image_to_string(keyword_image)

		line = keyword + line

	screens_data_file_2.write(line)

screens_data_file.close()