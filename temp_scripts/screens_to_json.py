import json

screens_data_file = open("./_out/screens.txt", "r")

obj = {
	"responses": None,
	"map": [],
}

obj["responses"] = json.loads(screens_data_file.readline().strip())

for i, line in enumerate(screens_data_file):
	[keyword, responses] = line.rstrip("\n").split(" ")

	if not keyword:
		print("Keyword #\x1b[31;1m{}\x1b[0m is blank".format(i + 1))

	obj["map"].append([keyword, [int(n) for n in responses.split(",")]])

screens_data_file.close()

screens_data_json_file = open("./_out/screens.json", "w")
screens_data_json_file.write(json.dumps(obj, separators=(",", ":")))
screens_data_json_file.close()