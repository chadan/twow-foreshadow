import re
import json
import math
from fractions import Fraction

MAX_USER_VOTES = 10
N_RESPONSES_PER_SCREEN = 6
vote_regex = r"\[([A-Z]{1,6}) ([A-F]{1,6})]"

percentile_divisor = N_RESPONSES_PER_SCREEN - 1

# Screens loading #####################################################################################################

screens_data_file = open("./_out/screens.txt", "r")

responses = json.loads(screens_data_file.readline().strip())
screens = {}
for i, line in enumerate(screens_data_file):
	[keyword, screen_responses] = line.rstrip("\n").split(" ")
	keyword = keyword.upper()

	screens[keyword] = [int(n) for n in screen_responses.split(",")]

screens_data_file.close()

# Vote recording ######################################################################################################

class Vote:
	def __init__(self, included_response_indexes: list, excluded_response_indexes: set):
		self.included_response_indexes = included_response_indexes
		self.excluded_response_indexes = excluded_response_indexes

comments_data_file = open("./_in/comments.json", "r", errors="ignore")

comments = json.load(comments_data_file)

users_to_votes = {}

def record_vote(keyword: str, response_sequence: str, user_votes):
	if len(user_votes) >= MAX_USER_VOTES:
		raise StopIteration # `continue` for comment loop

	keyword = keyword.upper()

	if keyword not in screens: # Invalid screen
		return

	screen = screens[keyword]
	response_indexes_on_screen = list(map(lambda char: ord(char) - ord("A"), list(response_sequence.upper())))

	# Record which responses this vote included
	included_response_indexes = [] # Order matters for included responses
	excluded_response_indexes = set(screen) # Order does not matter
	for response_index_on_screen in response_indexes_on_screen:
		response_index = screen[response_index_on_screen]

		if response_index in included_response_indexes: # Duplicate response in vote; invalid
			return

		excluded_response_indexes.discard(response_index)
		included_response_indexes.append(response_index)

	user_votes.append(Vote(included_response_indexes, excluded_response_indexes))

# Enumerate in reverse to deal with oldest votes first
for (comment_id, entry) in reversed(comments.items()):
	try:
		comment_content	= entry["content"]
		user_id	= entry["userId"]

		if user_id not in users_to_votes:
			user_votes = users_to_votes[user_id] = []
		else:
			user_votes = users_to_votes[user_id]

		prospective_votes = re.findall(vote_regex, comment_content, re.MULTILINE + re.IGNORECASE)
		for (keyword, response_sequence) in prospective_votes:
			record_vote(keyword, response_sequence, user_votes)
	
	except StopIteration: # raise StopIteration to simulate labeled continue
		pass

comments_data_file.close()

# Results calculations ################################################################################################

# Record all the partial percentiles for the responses

class PartialPercentile:
	"""Contains data about a single vote's influence on a response's ranking and percentile"""

	def __init__(self, percentile, weight_divisor):
		self.percentile = percentile
		self.weight_divisor = weight_divisor

	@property
	def ranking(self):
		# Formula derived by solving `1 - i / percentile_divisor` for `i`
		return -(self.percentile - 1) * percentile_divisor

	def vrd_influence(self):
		ranking = self.ranking

		if ranking % 1 == 0: # Ranking is an integer
			return [(int(ranking), 1)] # 100% influence to the current ranking

		else: # Ranking is distributed among two integer rankings
			ceil_influence = ranking % 1
			floor_influence = 1 - ceil_influence

			return [(int(math.floor(ranking)), floor_influence), (int(math.ceil(ranking)), ceil_influence)]

partial_percentiles_by_response = [] # Collection of `PartialPercentile`s for each response
for i in responses:
	partial_percentiles_by_response.append([])

for user_votes in users_to_votes.values():
	user_weight_divisor = len(user_votes) # Divisor to account for voting power being split across votes from a single user

	for vote in user_votes:
		# Iterate over the included responses first
		for i, response_index in enumerate(vote.included_response_indexes):
			partial_percentile = PartialPercentile(1 - Fraction(i, percentile_divisor), user_weight_divisor)
			partial_percentiles_by_response[response_index].append(partial_percentile)

		# Address the excluded responses
		split_percentile_remainder = max(Fraction(), Fraction(len(vote.excluded_response_indexes) - 1, 2 * percentile_divisor)) # Score given to all excluded responses
		for response_index in vote.excluded_response_indexes:
			partial_percentile = PartialPercentile(split_percentile_remainder, user_weight_divisor)
			partial_percentiles_by_response[response_index].append(partial_percentile)

# Calculate results from the partial percentiles

class Ranking:
	def __init__(self, content, avg_percentile, std_deviation, n_votes, n_voters, vrd):
		self.content = content
		self.avg_percentile = avg_percentile
		self.std_deviation = std_deviation
		self.n_votes = n_votes
		self.n_voters = n_voters
		self.vrd = vrd

rankings = []

for i, partial_percentiles in enumerate(partial_percentiles_by_response):
	# Average percentile and VRD
	cumsum_percentile = Fraction() # Empirical percentile cumsum
	cumsum_percentile_max = Fraction() # Maximum possible percentile cumsum (given these partial percentiles and their weights)
	vrd = [Fraction()] * N_RESPONSES_PER_SCREEN
	for partial_percentile in partial_percentiles:
		cumsum_percentile += Fraction(partial_percentile.percentile, partial_percentile.weight_divisor)
		cumsum_percentile_max += Fraction(1, partial_percentile.weight_divisor)

		# Update the VRD
		for (ranking, influence) in partial_percentile.vrd_influence():
			vrd[ranking] += Fraction(influence, partial_percentile.weight_divisor)

	avg_percentile = cumsum_percentile / cumsum_percentile_max

	# Standard deviation
	cumsum_variance_numerator = Fraction() # Numerator for the variance formula
	for partial_percentile in partial_percentiles: 
		cumsum_variance_numerator += Fraction((partial_percentile.percentile - avg_percentile) ** 2, partial_percentile.weight_divisor)

	std_deviation = math.sqrt(cumsum_variance_numerator / cumsum_percentile_max)

	n_votes = len(partial_percentiles)
	n_voters = cumsum_percentile_max

	content = responses[i]

	rankings.append(Ranking(content, float(avg_percentile), std_deviation, n_votes, n_voters, [float(n_placers) for n_placers in vrd]))

# Output

results_data_file = open("./_out/results.txt", "w")

for i, ranking in enumerate(sorted(rankings, key=lambda ranking: ranking.avg_percentile, reverse=True)):
	results_data_file.write(f"{ranking.avg_percentile}\t{ranking.std_deviation}\t{ranking.n_votes}\t{ranking.n_voters}\t{ranking.vrd}\t{ranking.content}\n")

	print(f"\x1b[32;1m#{i + 1}\x1b[0m : \x1b[97;1m{ranking.content}\x1b[0m")
	print(f"\t\x1b[90;1mAverage percentile : \x1b[0m{ranking.avg_percentile}")
	print(f"\t\x1b[90;1mStandard deviation : \x1b[0m{ranking.std_deviation}")
	print(f"\t\x1b[90;1m#Votes : \x1b[0m{ranking.n_votes}")
	print(f"\t\x1b[90;1m#Voters : \x1b[0m{ranking.n_voters}")
	print(f"\t\x1b[90;1mRating distribution : \x1b[0m{ranking.vrd}")
	print()

results_data_file.close()