import pytesseract
from PIL import Image
from difflib import get_close_matches
import json
pytesseract.pytesseract.tesseract_cmd = r"C:/Program Files/Tesseract-OCR/tesseract.exe"

KEYWORD_BOX_COORDS = (1600, 20, 1900, 120)
KEYWORD_BOX_COORDS_LARGE = (1500, 0, 1920, 140)
N_SCREENS = 1352

N_RESPONSES_PER_SCREEN = 6
RESPONSE_OFFSET = (140, 290)
RESPONSE_TEXT_HEIGHT = 40
RESPONSE_GAP_HEIGHT = 110

RESPONSE_SAMPLE_WIDTH = 120

SCREENS_FOLDER = "voting_screens"

screens_data_file = open("./_out/screens.txt", "w")

responses = []
responses_to_indexes = {}
# screens_to_responses = {}

is_first_screen = True

for i in range(1, N_SCREENS):
	# Get the current voting screen
	voting_screen_image_path = "./{}/{}.png".format(SCREENS_FOLDER, str(i).zfill(4))
	voting_screen_image = Image.open(voting_screen_image_path)

	# Get the keyword
	keyword_image = voting_screen_image.crop(KEYWORD_BOX_COORDS)
	keyword = pytesseract.image_to_string(keyword_image, config="-c tessedit_char_whitelist=ABCDEFGHIJKLMNOPQRSTUVWXYZ")

	if not keyword:
		# Retry with a larger search box if the keyword was not detected
		keyword_image = voting_screen_image.crop(KEYWORD_BOX_COORDS_LARGE)
		keyword = pytesseract.image_to_string(keyword_image)

	current_screen = []

	# Iterate through the responses on the screen
	for j in range(N_RESPONSES_PER_SCREEN):
		# Sample the response text
		response_image = voting_screen_image.crop((
			RESPONSE_OFFSET[0],
			RESPONSE_OFFSET[1] + j * RESPONSE_GAP_HEIGHT,
			RESPONSE_OFFSET[0] + RESPONSE_SAMPLE_WIDTH,
			RESPONSE_OFFSET[1] + j * RESPONSE_GAP_HEIGHT + RESPONSE_TEXT_HEIGHT,
		))

		# Not the best method. Switch to direct image comparison if possible

		response_text = pytesseract.image_to_string(response_image)

		# Record the response content if there is no existing data
		if is_first_screen:
			responses.append(response_text)
			responses_to_indexes[response_text] = j
			current_screen.append(j)
			continue

		# Get the response index
		try:
			# from exact match
			response_index = responses_to_indexes[response_text]
		except KeyError:
			# from fuzzy match, if there was no exact match
			closest_reference_text = get_close_matches(response_text, responses, 1)[0]
			response_index = responses_to_indexes[closest_reference_text]
		
		# Record the response index at its position on the screen
		current_screen.append(response_index)

	print("{}\t/ {}\t\t\x1b[32;1m\"{}\"\x1b[0m\t\x1b[90m{}\x1b[0m".format(i, N_SCREENS - 1, keyword, current_screen))

	if is_first_screen:
		json.dump(responses, screens_data_file)
		screens_data_file.write("\n")

	screens_data_file.write("{} {}\n".format(keyword, ",".join([str(response_index) for response_index in current_screen])))

	is_first_screen = False

screens_data_file.close()